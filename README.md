# Awesome space visualization tool

## url-collector

Microservice that gathers URLs concurrently from provider.

### Environment variables

- Populate .env file
- Default values
    - API_KEY=DEMO_KEY
    - CONCURRENT_REQUESTS=5
    - PORT=8080

### How to run

- Run with docker-compose

 ```
 sudo docker-compose build && sudo docker-compose up
 ```

- Run tests

 ```
 go test -cover ./...
 ```

### Exposed endpoints:

#### GET /pictures

| Query string parameter | Format |
| ------ | ------ |
| start_date | YYYY-MM-DD |
| end_date | YYYY-MM-DD |

| Response | Body |
| ------ | ------ |
| success | {“urls”: ["url1", "url2", "url3"]} |
| failure | {“error”: “error message”} |

## Project Structure

- `build/`: contains code for creating docker containers
- `cmd/`: contains microservice entries
- `internal/`
    - `core/`: contains domain, ports, services
    - `handlers/`: contains driver adapters
    - `mocks/`: contains mocks
    - `repositories/`: contains driven adapters
    - `servers/`: contains server startup code
- `pkg/`: contains utility packages
