package url_collector

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/ports"
	url_service "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/services/url-service"
	url_handler "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/handlers/url-handler"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/errors"
)

// Server represents url-collector server.
type Server struct {
	router   *gin.Engine
	settings *Settings
}

// Settings represents url-collector Server settings.
type Settings struct {
	ApiKey             string `env:"API_KEY" envDefault:"DEMO_KEY"`
	ConcurrentRequests int    `env:"CONCURRENT_REQUESTS" envDefault:"5"`
	Port               int    `env:"PORT" envDefault:"8080"`
}

// NewServer instantiates new Server and registers endpoints.
func NewServer(settings *Settings, urlRepository ports.URLRepository) *Server {
	urlService := url_service.NewService(urlRepository)
	urlHandler := url_handler.NewHTTPHandler(urlService)

	router := gin.New()
	urlHandler.RegisterEndpoints(&router.RouterGroup)

	return &Server{
		router:   router,
		settings: settings,
	}
}

// GetRouter returns router.
func (server *Server) GetRouter() *gin.Engine {
	return server.router
}

// Start runs router.
func (server *Server) Start() error {
	err := server.router.Run(fmt.Sprintf(":%d", server.settings.Port))
	if err != nil {
		return errors.WrapErrorf(err, "server.router.Run")
	}
	return nil
}
