package url_handler

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal"
	"net/url"
	"testing"
	"time"
)

func Test_getURLsQueryParams_bind(t *testing.T) {
	testCases := []struct {
		name      string
		startDate string
		endDate   string
		check     func(t *testing.T, err error)
	}{
		{
			name:      "start_date not provided",
			startDate: "",
			endDate:   time.Now().Format(internal.TimeLayoutYyyyMmDd),
			check: func(t *testing.T, err error) {
				assert.NotNil(t, err)
				assert.Equal(t, fmt.Sprintf(internal.FmtNotProvided, "start_date"), err.Error())
			},
		},
		{
			name:      "end_date not provided",
			startDate: time.Now().Format(internal.TimeLayoutYyyyMmDd),
			endDate:   "",
			check: func(t *testing.T, err error) {
				assert.NotNil(t, err)
				assert.Equal(t, fmt.Sprintf(internal.FmtNotProvided, "end_date"), err.Error())
			},
		},
		{
			name:      "start_date invalid format",
			startDate: time.Now().String(),
			endDate:   time.Now().Format(internal.TimeLayoutYyyyMmDd),
			check: func(t *testing.T, err error) {
				assert.NotNil(t, err)
				assert.Equal(t, fmt.Sprintf(internal.FmtDoesNotMatchFormat, "start_date", internal.TimeFormatYyyyMmDd), err.Error())
			},
		},
		{
			name:      "end_date invalid format",
			startDate: time.Now().Format(internal.TimeLayoutYyyyMmDd),
			endDate:   time.Now().String(),
			check: func(t *testing.T, err error) {
				assert.NotNil(t, err)
				assert.Equal(t, fmt.Sprintf(internal.FmtDoesNotMatchFormat, "end_date", internal.TimeFormatYyyyMmDd), err.Error())
			},
		},
		{
			name:      "start_date and end_date valid formats",
			startDate: time.Now().Format(internal.TimeLayoutYyyyMmDd),
			endDate:   time.Now().Format(internal.TimeLayoutYyyyMmDd),
			check: func(t *testing.T, err error) {
				assert.Nil(t, err)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			qp := getURLsQueryParams{}
			values := url.Values{}
			values.Set("start_date", tc.startDate)
			values.Set("end_date", tc.endDate)

			err := qp.bind(values)
			tc.check(t, err)
		})
	}
}

func Test_getURLsQueryParams_validate(t *testing.T) {
	testCases := []struct {
		name      string
		startDate time.Time
		endDate   time.Time
		check     func(t *testing.T, err error)
	}{
		{
			name:      "end_date greater then today",
			startDate: time.Now(),
			endDate:   time.Now().AddDate(0, 0, 1),
			check: func(t *testing.T, err error) {
				assert.NotNil(t, err)
				assert.Equal(t, fmt.Sprintf(internal.FmtGreaterThen, "end_date", "today"), err.Error())
			},
		},
		{
			name:      "end_date before start_date",
			startDate: time.Now(),
			endDate:   time.Now().AddDate(0, 0, -1),
			check: func(t *testing.T, err error) {
				assert.NotNil(t, err)
				assert.Equal(t, fmt.Sprintf(internal.FmtGreaterThen, "start_date", "end_date"), err.Error())
			},
		},
		{
			name:      "start_date and end_date valid",
			startDate: time.Now(),
			endDate:   time.Now(),
			check: func(t *testing.T, err error) {
				assert.Nil(t, err)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			qp := getURLsQueryParams{
				StartDate: tc.startDate,
				EndDate:   tc.endDate,
			}

			err := qp.validate()
			tc.check(t, err)
		})
	}
}