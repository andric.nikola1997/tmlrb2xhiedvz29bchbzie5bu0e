package url_handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/ports"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/errors"
	ginPkg "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/gin"
)

// HTTPHandler http handler for url requests.
type HTTPHandler struct {
	urlService ports.URLService
}

// NewHTTPHandler instantiates new HTTPHandler.
func NewHTTPHandler(urlsService ports.URLService) *HTTPHandler {
	return &HTTPHandler{
		urlService: urlsService,
	}
}

// RegisterEndpoints registers all url endpoints.
func (hdl *HTTPHandler) RegisterEndpoints(rg *gin.RouterGroup) {
	rg.GET("/pictures", hdl.getURLs)
}

// getURLs endpoint that returns GetURLsResponse on success.
func (hdl *HTTPHandler) getURLs(c *gin.Context) {
	qp := getURLsQueryParams{}
	if err := qp.bind(c.Request.URL.Query()); err != nil {
		errors.Log(errors.WrapErrorf(err, "getURLsQueryParams.bind"), "HTTPHandler.getURLs")
		ginPkg.ThrowStatusBadRequest(err.Error(), c)
		return
	}
	if err := qp.validate(); err != nil {
		errors.Log(errors.WrapErrorf(err, "getURLsQueryParams.validate"), "HTTPHandler.getURLs")
		ginPkg.ThrowStatusBadRequest(err.Error(), c)
		return
	}
	urls, err := hdl.urlService.GetURLs(qp.StartDate, qp.EndDate)
	if err != nil {
		errors.Log(errors.WrapErrorf(err, "ports.URLService.GetURLs"), "HTTPHandler.getURLs")
		ginPkg.ThrowStatusInternalServerError(c)
		return
	}
	ginPkg.ThrowStatusOK(GetURLsResponse{URLs: urls}, c)
	return
}
