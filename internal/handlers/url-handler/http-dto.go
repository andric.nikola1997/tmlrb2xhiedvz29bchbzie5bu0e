package url_handler

import "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/domain"

type GetURLsResponse struct {
	URLs domain.URLs `json:"urls"`
}
