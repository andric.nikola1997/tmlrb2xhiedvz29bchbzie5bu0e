package url_handler

import (
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/errors"
	"net/url"
	"time"
)

// getURLsQueryParams query parameters of HTTPHandler.getURLs.
type getURLsQueryParams struct {
	StartDate time.Time
	EndDate   time.Time
}

// bind used to bind query parameters into getURLsQueryParams in proper way.
func (qp *getURLsQueryParams) bind(queryParams url.Values) (err error) {
	startDate := queryParams.Get("start_date")
	if startDate == "" {
		return errors.NewErrorf(internal.FmtNotProvided, "start_date")
	}
	endDate := queryParams.Get("end_date")
	if endDate == "" {
		return errors.NewErrorf(internal.FmtNotProvided, "end_date")
	}
	qp.StartDate, err = time.Parse(internal.TimeLayoutYyyyMmDd, startDate)
	if err != nil {
		return errors.NewErrorf(internal.FmtDoesNotMatchFormat, "start_date", internal.TimeFormatYyyyMmDd)
	}
	qp.EndDate, err = time.Parse(internal.TimeLayoutYyyyMmDd, endDate)
	if err != nil {
		return errors.NewErrorf(internal.FmtDoesNotMatchFormat, "end_date", internal.TimeFormatYyyyMmDd)
	}
	return nil
}

// validate checks if query param are logical.
func (qp *getURLsQueryParams) validate() (err error) {
	today := time.Now().UTC()
	if qp.EndDate.After(today) {
		return errors.NewErrorf(internal.FmtGreaterThen, "end_date", "today")
	}
	if qp.EndDate.Before(qp.StartDate) {
		return errors.NewErrorf(internal.FmtGreaterThen, "start_date", "end_date")
	}
	return nil
}
