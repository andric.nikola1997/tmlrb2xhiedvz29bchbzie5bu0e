package url_handler_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/domain"
	url_handler "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/handlers/url-handler"
	mock_repositories "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/mocks/repositories"
	url_collector "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/servers/url-collector"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_HTTPHandler_getURL(t *testing.T) {
	randomUrls := getRandomURLs(10)

	testCases := []struct {
		name          string
		startDate     string
		endDate       string
		buildStubs    func(urlRepository *mock_repositories.MockURLRepository)
		checkResponse func(t *testing.T, recorder *httptest.ResponseRecorder)
	}{
		{
			name:      "valid request",
			startDate: "2019-06-02",
			endDate:   "2019-06-12",
			buildStubs: func(urlRepository *mock_repositories.MockURLRepository) {
				urlRepository.EXPECT().
					GetURLs(gomock.Any(), gomock.Any()).
					Times(1).
					Return(randomUrls, nil)
			},
			checkResponse: func(t *testing.T, recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusOK, recorder.Code)
				requireBodyMatchURLs(t, recorder.Body, randomUrls)
			},
		},
		{
			name:      "internal server error",
			startDate: "2019-06-02",
			endDate:   "2019-06-10",
			buildStubs: func(urlRepository *mock_repositories.MockURLRepository) {
				urlRepository.EXPECT().
					GetURLs(gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil, errors.New("err example"))
			},
			checkResponse: func(t *testing.T, recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusInternalServerError, recorder.Code)
			},
		},
		{
			name:       "bad request date range",
			startDate:  "2019-06-12",
			endDate:    "2019-06-10",
			buildStubs: func(urlRepository *mock_repositories.MockURLRepository) {},
			checkResponse: func(t *testing.T, recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusBadRequest, recorder.Code)
			},
		},
		{
			name:       "bad request date format",
			startDate:  "2019_06_02",
			endDate:    "2019_06_10",
			buildStubs: func(urlRepository *mock_repositories.MockURLRepository) {},
			checkResponse: func(t *testing.T, recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusBadRequest, recorder.Code)
			},
		},
		{
			name:       "bad request date missing",
			startDate:  "2019-06-02",
			endDate:    "",
			buildStubs: func(urlRepository *mock_repositories.MockURLRepository) {},
			checkResponse: func(t *testing.T, recorder *httptest.ResponseRecorder) {
				require.Equal(t, http.StatusBadRequest, recorder.Code)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			urlRepository := mock_repositories.NewMockURLRepository(ctrl)
			tc.buildStubs(urlRepository)

			server := url_collector.NewServer(&url_collector.Settings{}, urlRepository)
			recorder := httptest.NewRecorder()

			url := fmt.Sprintf("/pictures?start_date=%s&end_date=%s", tc.startDate, tc.endDate)
			request, err := http.NewRequest(http.MethodGet, url, nil)
			require.NoError(t, err)

			server.GetRouter().ServeHTTP(recorder, request)
		})
	}
}

func getRandomURLs(num int) domain.URLs {
	urls := domain.URLs{}
	for i := 0; i < num; i++ {
		urls = append(urls, domain.URL(fmt.Sprintf("https://fake-url.com/%d", i)))
	}
	return urls
}

func requireBodyMatchURLs(t *testing.T, body *bytes.Buffer, urls domain.URLs) {
	data, err := ioutil.ReadAll(body)
	require.NoError(t, err)

	var gotUrls url_handler.GetURLsResponse
	err = json.Unmarshal(data, &gotUrls)
	require.NoError(t, err)
	require.Equal(t, urls, gotUrls.URLs)
}
