package url_service

import (
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/domain"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/ports"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/errors"
	"time"
)

// service implementation of ports.URLService.
type service struct {
	urlRepository ports.URLRepository
}

// NewService instantiates new service.
func NewService(urlRepository ports.URLRepository) *service {
	return &service{
		urlRepository: urlRepository,
	}
}

// GetURLs returns domain.URLs.
func (srv *service) GetURLs(startDate, endDate time.Time) (domain.URLs, error) {
	urls, err := srv.urlRepository.GetURLs(startDate, endDate)
	if err != nil {
		return nil, errors.WrapErrorf(err, "ports.URLRepository.GetURLs")
	}
	return urls, nil
}
