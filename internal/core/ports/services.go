package ports

import (
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/domain"
	"time"
)

type URLService interface {
	GetURLs(startDate, endDate time.Time) (domain.URLs, error)
}