package url_repository

import (
	"encoding/json"
	"fmt"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/domain"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/concurrency"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/errors"
	httpPkg "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/http"
	timePkg "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/time"
	"net/http"
	"sync"
	"time"
)

const (
	baseURL = "https://api.nasa.gov/planetary/apod"
)

// nasaApodApi implements ports.URLRepository.
type nasaApodApi struct {
	url  string
	pool *concurrency.GoroutinePool
}

// NewNasaApodApi instantiates new nasaApodApi.
func NewNasaApodApi(apiKey string, pool *concurrency.GoroutinePool) *nasaApodApi {
	return &nasaApodApi{
		url:  fmt.Sprintf("%s?api_key=%s", baseURL, apiKey),
		pool: pool,
	}
}

// GetURLs gets domain.URLs.
func (n *nasaApodApi) GetURLs(startDate, endDate time.Time) (domain.URLs, error) {
	var fns []concurrency.GenericFunctionWithParams

	urls := &domain.URLs{}
	mutex := &sync.Mutex{}

	for _, date := range timePkg.GetDatesInRange(startDate, endDate) {
		fn := concurrency.GenericFunctionWithParams{
			Function: func(urls *domain.URLs, mutex *sync.Mutex, date string) error {
				resp, err := n.getResponseForDate(date)
				if err != nil {
					return errors.WrapErrorf(err, "nasaApodApi.getResponseForDate")
				}

				mutex.Lock()
				*urls = append(*urls, domain.URL(resp.URL))
				mutex.Unlock()

				return nil
			},
			Params: []interface{}{urls, mutex, date.Format(internal.TimeLayoutYyyyMmDd)},
		}
		fns = append(fns, fn)
	}

	err := n.pool.Exec(fns)
	if err != nil {
		return nil, errors.WrapErrorf(err, "concurrency.GoroutinePool.Exec")
	}

	return *urls, nil
}

// nasaApodApiObject represents single response object from nasa apod api.
type nasaApodApiObject struct {
	Copyright      string `json:"copyright"`
	Date           string `json:"date"`
	Explanation    string `json:"explanation"`
	Hdurl          string `json:"hdurl"`
	MediaType      string `json:"media_type"`
	ServiceVersion string `json:"service_version"`
	Title          string `json:"title"`
	URL            string `json:"url"`
}

// getResponseForDate returns nasaApodApiObject from nasa apod api.
func (n *nasaApodApi) getResponseForDate(date string) (nasaApodApiObject, error) {
	req, err := http.NewRequest(http.MethodGet, n.url, nil)
	if err != nil {
		return nasaApodApiObject{}, errors.WrapErrorf(err, "http.NewRequest")
	}
	q := req.URL.Query()
	q.Add("date", date)
	req.URL.RawQuery = q.Encode()

	bodyBytes, err := httpPkg.ExecuteRequest(req)
	if err != nil {
		return nasaApodApiObject{}, errors.WrapErrorf(err, "httpPkg.ExecuteRequest")
	}

	resp := nasaApodApiObject{}
	err = json.Unmarshal(bodyBytes, &resp)
	if err != nil {
		return nasaApodApiObject{}, errors.WrapErrorf(err, "json.Unmarshal")
	}
	return resp, nil
}
