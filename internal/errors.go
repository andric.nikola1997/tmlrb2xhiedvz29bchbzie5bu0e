package internal

var (
	FmtNotProvided = "%s not provided"
	FmtGreaterThen = "%s greater then %s"
	FmtDoesNotMatchFormat = "%s does not match format %s"
)