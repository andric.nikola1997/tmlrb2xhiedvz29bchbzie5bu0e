package http

import (
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/errors"
	"io/ioutil"
	"net/http"
)

// ExecuteRequest executes request and returns body bytes if request was successful.
func ExecuteRequest(req *http.Request) (respBodyBytes []byte, err error) {
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.WrapErrorf(err, "client.Do")
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.NewErrorf("status code is not 200")
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.WrapErrorf(err, "ioutil.ReadAll")
	}
	return bodyBytes, nil
}
