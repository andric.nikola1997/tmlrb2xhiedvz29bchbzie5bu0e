package errors

import (
	"fmt"
	"log"
)

// Error represents an error that could be wrapping another error, it includes a code for determining what
// triggered the error.
type Error struct {
	ErrOrig error
	ErrMsg  string
}

// WrapErrorf returns a wrapped error.
func WrapErrorf(orig error, format string, a ...interface{}) error {
	return &Error{
		ErrOrig: orig,
		ErrMsg:  fmt.Sprintf(format, a...),
	}
}

// NewErrorf instantiates a new error.
func NewErrorf(format string, a ...interface{}) error {
	return WrapErrorf(nil, format, a...)
}

// Error returns the message, when wrapping errors the wrapped error is returned.
func (e *Error) Error() string {
	if e.ErrOrig != nil {
		return fmt.Sprintf("%s: %v", e.ErrMsg, e.ErrOrig)
	}
	return e.ErrMsg
}

// Unwrap returns the wrapped error, if any.
func (e *Error) Unwrap() error {
	return e.ErrOrig
}

// Log prints error without exit.
func Log(err error, from string) {
	log.Printf("[ERROR] %s: %s", from, err)
}

// LogFatal prints error with exit.
func LogFatal(err error, from string) {
	log.Fatalf("[FATAL] %s: %s", from, err)
}
