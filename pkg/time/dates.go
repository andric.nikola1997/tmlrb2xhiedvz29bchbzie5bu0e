package time

import (
	"time"
)

// GetDatesInRange returns dates for given range.
func GetDatesInRange(startDate, endDate time.Time) []time.Time {
	var dates []time.Time
	for d := startDate; d.After(endDate) == false; d = d.AddDate(0, 0, 1) {
		dates = append(dates, d)
	}
	return dates
}