package gin

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type errorResponse struct {
	Error string `json:"error"`
}

func ThrowStatusOK(i interface{}, c *gin.Context) {
	if i != nil {
		c.JSON(http.StatusOK, i)
		return
	}
}

func ThrowStatusBadRequest(msg string, c *gin.Context) {
	if msg != "" {
		c.AbortWithStatusJSON(http.StatusBadRequest,
			errorResponse{
				Error: msg,
			})
		return
	}

	c.AbortWithStatusJSON(http.StatusBadRequest, errorResponse{
		Error: "bad request",
	})
}

func ThrowStatusInternalServerError(c *gin.Context) {
	c.AbortWithStatusJSON(http.StatusInternalServerError, errorResponse{
		Error: "internal server error",
	})
}
