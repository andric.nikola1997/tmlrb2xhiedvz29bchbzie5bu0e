package settings

import (
	"errors"
	"github.com/caarlos0/env"
)

// LoadEnvironmentVariables loads environment variables into destination.
func LoadEnvironmentVariables(dest interface{}) error {
	err := env.Parse(dest)
	if err != nil {
		return errors.New("env.Parse")
	}
	return nil
}