package concurrency

import (
	"context"
	"reflect"
	"sync"
)

// GoroutinePool represents resource that manages limited number of goroutines.
type GoroutinePool struct {
	guard *chan struct{}
}

// NewGoroutinePool instantiates new GoroutinePool
func NewGoroutinePool(maxGoroutines int) *GoroutinePool {
	guard := make(chan struct{}, maxGoroutines)
	return &GoroutinePool{
		guard: &guard,
	}
}

// GenericFunctionWithParams represents generic function with its arguments.
type GenericFunctionWithParams struct {
	Function interface{}
	Params   []interface{}
}

// Exec executes multiple GenericFunctionWithParams concurrently,
// if one of them fails it makes sure that other stop with execution.
func (gp *GoroutinePool) Exec(functions []GenericFunctionWithParams) error {
	fatalErrors := make(chan error, 1)
	wgDone := make(chan bool, 1)
	wg := &sync.WaitGroup{}

	ctx, cancel := context.WithCancel(context.Background())
	defer func() { cancel() }()

	for ix := range functions {
		*gp.guard <- struct{}{}
		wg.Add(1)
		go func(fun GenericFunctionWithParams) {
			defer func() {
				wg.Done()
				<-*gp.guard
			}()

			select {
			case <-ctx.Done():
				return
			default:
				args := make([]reflect.Value, len(fun.Params))
				for i := range fun.Params {
					args[i] = reflect.ValueOf(fun.Params[i])
				}
				f := reflect.ValueOf(fun.Function)
				values := f.Call(args)

				for _, value := range values {
					if value.Type().String() == "error" && !value.IsNil() && len(fatalErrors) != cap(fatalErrors) {
						fatalErrors <- value.Interface().(error)
						cancel()
						return
					}
				}
			}
		}(functions[ix])
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()

	select {
	case <-wgDone:
		break
	case err := <-fatalErrors:
		return err
	}
	return nil
}
