package main

import (
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/repositories/url-repository"
	url_collector "gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/servers/url-collector"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/concurrency"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/errors"
	"gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/pkg/settings"
)

var serverSettings = &url_collector.Settings{}

func init() {
	err := settings.LoadEnvironmentVariables(serverSettings)
	if err != nil {
		errors.LogFatal(errors.WrapErrorf(err, "settings.LoadEnvironmentVariables"), "main.init")
	}
}

func main() {
	goroutinePool := concurrency.NewGoroutinePool(serverSettings.ConcurrentRequests)
	urlRepository := url_repository.NewNasaApodApi(serverSettings.ApiKey, goroutinePool)

	err := url_collector.NewServer(serverSettings, urlRepository).Start()
	if err != nil {
		errors.LogFatal(errors.WrapErrorf(err, "NewServer.Start"), "main.main")
	}
}