module gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e

go 1.15

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/gin-gonic/gin v1.7.4
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.7.0
)
