mock:
	mockgen -package mock_repositories -destination internal/mocks/repositories/url-repository.go gitlab.com/andric.nikola1997/tmlrb2xhiedvz29bchbzie5bu0e/internal/core/ports URLRepository
test:
	go test -cover ./...
docker-compose:
	sudo docker-compose build && sudo docker-compose up